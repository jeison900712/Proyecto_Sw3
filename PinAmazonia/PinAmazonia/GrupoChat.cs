﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PinAmazonia
{
    public class GrupoChat
    {
        private string nombre_user;
        private string mensaje;

        public string Nombre_user {
            get { return nombre_user; }
            set { nombre_user = value; }
        }

        public string Mensaje {
            get { return mensaje; }
            set { mensaje = value; }

          }
        public GrupoChat() { }

        public GrupoChat(string nombre_user, string mensaje) {
            Nombre_user = nombre_user;
            Mensaje = mensaje;

        
        }
    }
}
