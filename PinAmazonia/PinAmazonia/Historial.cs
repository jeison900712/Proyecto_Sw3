﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinAmazonia.Models
{
    public class Historial
    {
        private String fecha_histo;
        private String actividad;
        private Perfil fk_perfil;

        public string Fecha_histo {
            get { return fecha_histo; }
            set { fecha_histo = value; }

        }

        public string Actividad {
            get { return actividad; }
            set { actividad = value; }
        }

        public Perfil Fk_perfil{
            get { return fk_perfil; }
            set { fk_perfil = value; }

    }

        public Historial() { }

        public Historial(String fecha_histo, String actividad, Perfil fk_perfil){
            Fecha_histo = fecha_histo;
            Actividad = actividad;
            Fk_perfil = fk_perfil; 
        
        }
    
    }
}