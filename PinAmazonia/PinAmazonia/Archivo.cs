﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinAmazonia.Models
{
    public class Archivo
    {
        private String archivo;
        private String formato;
        
        
        public string Archi
        {
            get { return archivo; }
            set { archivo = value; }

        }

        public string Formato
        {
            get { return formato; }
            set { formato = value; }

        }

        public Archivo() { }

        public Archivo(String archivo, String formato) {

            Archi = archivo;
            Formato = formato;

        }
    }

     
}