﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinAmazonia
{
    public class Mensaje
    {
        private String remitente;
        private String destinatario;
        private String mensaje;
        private String archivo;

        public string Remitente
        {
            get { return remitente; }
            set { remitente = value; }

        }
        public string Destinatario
        {
            get { return destinatario; }
            set { destinatario = value; }
        }
        public string Msg
        {
            get { return mensaje; }
            set { mensaje = value; }
        }
        public string Archivo
        {
            get { return archivo; }
            set { archivo = value; }
        }

        public Mensaje() { }

        public Mensaje(String remitente, String mensaje)
        {

            Remitente = remitente;
            Destinatario = destinatario;
            Msg = mensaje;
            Archivo = archivo;

        }
    }
}