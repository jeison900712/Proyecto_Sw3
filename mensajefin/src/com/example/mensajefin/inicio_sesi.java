package com.example.mensajefin;


import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class inicio_sesi extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inicio_sesion);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public void Sesion(View v) {
		Thread nt = new Thread() {
			String res;
			EditText nom_sesi = (EditText) findViewById(R.id.nom_user);
			EditText passwor = (EditText) findViewById(R.id.editText1);

			@Override
			public void run() {

				String NAMESPACE = "http://demo.pinamazonia.org/";
				String URL = "http://172.20.10.2/prueba2/PinWS.asmx?WSDL";
				String METHOD_NAME = "getSesion";
				String SOAP_ACTION = "http://demo.pinamazonia.org/getSesion";
				
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("nombre_usuario",nom_sesi.getText().toString());
				request.addProperty("pwd",passwor.getText().toString());
				

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;

				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(URL);

				try {
					transporte.call(SOAP_ACTION, envelope);
					SoapPrimitive resultado_xml = (SoapPrimitive) envelope
							.getResponse();
					res = resultado_xml.toString();

				} catch (HttpResponseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						Toast.makeText(inicio_sesi.this, res,
								Toast.LENGTH_LONG).show();
						TextView result =(TextView) findViewById(R.id.textView3);
						result.setText(res);

					}

				});
			}
		};

		nt.start();

	}
}
