package com.example.mensajefin;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
   
       
    public void EnviarOnclick(View v) {
		Thread nt = new Thread() {
			
			String res;
			
			EditText remi = (EditText) findViewById(R.id.remi);
			EditText desti = (EditText) findViewById(R.id.desti);
			EditText mensa = (EditText) findViewById(R.id.editText3);


			@Override
			public void run() {

				String NAMESPACE = "http://demo.pinamazonia.org/";
				String URL = "http://192.168.1.33/prueba2/PinWS.asmx?WSDL";
				String METHOD_NAME = "insertar_mensaje";
				String SOAP_ACTION = "http://demo.pinamazonia.org/insertar_mensaje";
				
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("remi",remi.getText().toString());
				request.addProperty("desti",desti.getText().toString());
				request.addProperty("mensa",mensa.getText().toString());
				
				

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;

				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(URL);

				try {
					transporte.call(SOAP_ACTION, envelope);
					SoapPrimitive resultado_xml = (SoapPrimitive) envelope
							.getResponse();
					res = resultado_xml.toString();

				} catch (HttpResponseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						Toast.makeText(MainActivity.this, res,
								Toast.LENGTH_LONG).show();
						TextView result =(TextView) findViewById(R.id.textView3);
						result.setText(res);

					}

				});
			}
		};

		nt.start();

	}
}
